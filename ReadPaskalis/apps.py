from django.apps import AppConfig


class ReadpaskalisConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ReadPaskalis'
