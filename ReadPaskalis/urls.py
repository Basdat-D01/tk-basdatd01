from ReadPaskalis import views
from django.urls import path

app_name = 'ReadPaskalis'
urlpatterns = [
    path('warna-kulit/', views.getWarnaKulit, name = 'Warna Kulit'),
    path('level/', views.getLevel, name = 'Level'),
    path('menggunakan-apparel/', views.getMenggunakanApparel, name = 'Menggunakan Apparel'),
    path('menggunakan-apparel/create/', views.createMenggunakanApparel, name = 'Create Menggunakan Apparel'),
    path('menggunakan-apparel/delete/<str:id>', views.deleteMenggunakanApparel, name = 'Delete Menggunakan Apparel'),
    path('warna-kulit/create/', views.createWarnaKulit, name = 'Create Warna Kulit'),
    path('warna-kulit/delete/<str:kode>', views.deleteWarnaKulit, name = 'Delete Warna Kulit'),
    path('level/create/', views.createLevel, name = 'Create Level'),
    path('level/update/<int:level>', views.updateLevel, name = 'Update Level'),
    path('level/delete/<int:level>', views.deleteLevel, name = 'Delete Level'),
    ]