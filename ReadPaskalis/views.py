from django.shortcuts import render
from Auth.views import *
from Auth.urls import *
from django.shortcuts import render, redirect
import django.db as db
from django.db import connection
# Create your views here.

def getWarnaKulit(request):
    userUsername = checkLoggedIn(request)

    if not userUsername:
        return redirect('mainApp:Index')

    if not cekAdmin(request, userUsername):

        return redirect('/')

    cursor = connection.cursor()
    cursor.execute("set search_path to the_cims")

    cursor.execute("SELECT * FROM WARNA_KULIT")
    warna_kulit = cursor.fetchall()


    return render(request, 'WarnaKulit.html', {'warna_kulit' : warna_kulit})

def getLevel(request):
    userUsername = checkLoggedIn(request)

    if not userUsername:
        return redirect('mainApp:Index')

    if not cekAdmin(request, userUsername):

        return redirect('/')

    cursor = connection.cursor()
    cursor.execute("set search_path to the_cims")

    cursor.execute("SELECT * FROM LEVEL ORDER BY level ASC")
    level = cursor.fetchall()


    return render(request, 'Level.html', {'level' : level})

def getMenggunakanApparel(request):
    userUsername = checkLoggedIn(request)

    if not userUsername:
        return redirect('mainApp:Index')

    if not cekAdmin(request, userUsername):

        return redirect('/')

    cekRole = request.session['username']
    role = cekAdmin(request, cekRole)

    if role == "Admin":

        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")

        cursor.execute("SELECT MA.username_pengguna, MA.nama_tokoh, KJB.nama ,A.warna_apparel, A.nama_pekerjaan, A.kategori_apparel FROM MENGGUNAKAN_APPAREL MA, APPAREL A, KOLEKSI_JUAL_BELI KJB WHERE MA.id_koleksi = A.id_koleksi AND KJB.id_koleksi = A.id_koleksi")
        apparel = cursor.fetchall()

    elif role == "Pemain":

        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")

        cursor.execute("SELECT MA.username_pengguna, MA.nama_tokoh, KJB.nama ,A.warna_apparel, A.nama_pekerjaan, A.kategori_apparel, A.id_koleksi FROM MENGGUNAKAN_APPAREL MA, APPAREL A, KOLEKSI_JUAL_BELI KJB WHERE MA.id_koleksi = A.id_koleksi AND KJB.id_koleksi = A.id_koleksi AND MA.username_pengguna = %s", [cekRole])
        apparel = cursor.fetchall()

    return render(request, 'MenggunakanApparel.html', {'apparel' : apparel})

def createWarnaKulit(request):
    userUsername = checkLoggedIn(request)
    cekRole = request.session['username']
    role = cekAdmin(request, cekRole)
    data = request.POST
    response = {}

    if not userUsername:
        return redirect('mainApp:Index')

    if role == "Pemain":

        return redirect('/warna-kulit/')

    else:

        if request.method == 'POST':
            message = " "
            kode = data['kode']

            try:
                cursor = connection.cursor()
                cursor.execute("set search_path to the_cims")
                cursor.execute(
                    "INSERT INTO WARNA_KULIT VALUES (%s)", [kode]
                )
                message = "Warna Kulit berhasil ditambahkan!"

            except db.Error as e:
                message = e

            return render(request, 'CreateWarnaKulit.html', {'message' : message})

        else:

            return render(request, 'CreateWarnaKulit.html', response)

def createLevel(request):
    userUsername = checkLoggedIn(request)
    cekRole = request.session['username']
    role = cekAdmin(request, cekRole)
    data = request.POST
    response = {}

    if not userUsername:
        return redirect('mainApp:Index')

    if role == "Pemain":

        return redirect('/level/')

    else:

        if request.method == 'POST':
            message = " "
            level = int(data['level'])
            xp = int(data['xp'])

            try:
                cursor = connection.cursor()
                cursor.execute("set search_path to the_cims")
                cursor.execute(
                    "INSERT INTO LEVEL VALUES (%s, %s)", [level, xp]
                )
                message = "Level berhasil ditambahkan!"

            except db.Error as e:
                message = e

            return render(request, 'CreateLevel.html', {'message' : message})

        else:

            return render(request, 'CreateLevel.html', response)

def updateLevel(request, level):

    userUsername = checkLoggedIn(request)
    cekRole = request.session['username']
    role = cekAdmin(request, cekRole)
    data = request.POST

    if not userUsername:
        return redirect('mainApp:Index')

    if role == "Pemain":

        return redirect('/level/')

    else:

        if request.method == 'POST':
            message = " "
            xp = int(data['xp'])

            try:
                cursor = connection.cursor()
                cursor.execute("set search_path to the_cims")
                cursor.execute(
                "UPDATE LEVEL set xp = %s where level = %s", [xp, level]
            )
                message = "Level berhasil diupdate!"

            except db.Error as e:
                message = e

            return render(request, 'UpdateLevel.html', {'message' : message, 'level': level})

        else:

            return render(request, 'UpdateLevel.html', {'level': level})

    
def deleteWarnaKulit(request, kode):
    cekRole = request.session['username']
    role = cekAdmin(request, cekRole)

    message = " "

    if role == "Pemain":

        return redirect('/menggunakan-apparel/')

    message = " "
    kodeWarna = "#" + kode

    try:
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")
        cursor.execute(
            "DELETE FROM WARNA_KULIT WHERE kode = %s", [kodeWarna]
        )
        message = "Warna Kulit berkode " + kodeWarna + " berhasil dihapus!"

    except db.Error as e:
        message = e

    userUsername = checkLoggedIn(request)

    if not userUsername:
        return redirect('mainApp:Index')

    if not cekAdmin(request, userUsername):

        return redirect('/')

    cursor = connection.cursor()
    cursor.execute("set search_path to the_cims")

    cursor.execute("SELECT * FROM WARNA_KULIT")
    warna_kulit = cursor.fetchall()


    return render(request, 'WarnaKulit.html', {'warna_kulit' : warna_kulit , 'message' : message})

def deleteLevel(request, level):
    cekRole = request.session['username']
    role = cekAdmin(request, cekRole)

    message = " "

    if role == "Pemain":

        return redirect('/menggunakan-apparel/')

    try:
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")
        cursor.execute(
            "DELETE FROM LEVEL WHERE level = %s", [level]
        )
        message = "Level " + str(level) + " berhasil dihapus"

    except db.Error as e:
        message = e

    return redirect('/level/')

def createMenggunakanApparel(request):
    userUsername = checkLoggedIn(request)
    cekRole = request.session['username']
    role = cekAdmin(request, cekRole)
    data = request.POST
    response = {}

    if not userUsername:
        return redirect('mainApp:Index')

    if role == "Admin":

        return redirect('/menggunakan-apparel/')

    else:

        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")

        cursor.execute("SELECT * FROM TOKOH WHERE username_pengguna = %s", [cekRole])
        tokoh = cursor.fetchall()
        response['tokoh'] = tokoh

        cursor.execute("SELECT * FROM APPAREL")
        apparel = cursor.fetchall()
        response['apparel'] = apparel

        if request.method == 'POST':
            message = " "
            tokoh = data["nama_tokoh"]
            apparel = data["id_koleksi"]

            try:
                cursor = connection.cursor()
                cursor.execute("set search_path to the_cims")
                cursor.execute(
                    "INSERT INTO MENGGUNAKAN_APPAREL VALUES (%s, %s, %s)", [cekRole, tokoh, apparel]
                )
                message = str(tokoh) + " berhasil menggunakan apparel dengan id " + str(apparel)

            except db.Error as e:
                message = e

            return render(request, 'CreateMenggunakanApparel.html', {'message' : message})

        else:

            return render(request, 'CreateMenggunakanApparel.html', response)

def deleteMenggunakanApparel(request, id):
    cekRole = request.session['username']
    role = cekAdmin(request, cekRole)

    if role == "Admin":

        return redirect('/menggunakan-apparel/')

    message = " "

    try:
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")
        cursor.execute(
            "DELETE FROM MENGGUNAKAN_APPAREL WHERE id_koleksi = %s AND username_pengguna = %s", [id, cekRole]
        )
        message = "Apparel dengan id " + str(id) + " berhasil dilepas"

    except db.Error as e:
        message = e

    return redirect('/menggunakan-apparel/')

    