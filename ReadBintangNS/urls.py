from ReadBintangNS import views
from django.urls import path

app_name = 'ReadBintangNS'
urlpatterns = [
    path('menggunakan-barang/', views.GetMenggunakanBarang, name = 'Menggunakan Barang'),
    path('pekerjaan/', views.GetPekerjaan, name = 'Pekerjaan'),
    path('bekerja/', views.GetBekerja, name = 'Bekerja'),
    path('tambahpekerjaan/', views.TambahPekerjaan, name = 'Tambah Pekerjaan'),
    path ('list-tokoh/', views.GetTokohPengguna, name = 'List Tokoh Pengguna'),
    path ('create-tokoh/', views.TambahTokoh, name = 'Tambah Tokoh'),
    path('list-tokoh/update/<str:nama_tokoh>', views.UpdateTokoh, name = 'Update Tokoh'),
    path('list-tokoh/detail/<str:nama_tokoh>', views.DetailTokohPengguna, name = 'Detail Tokoh'),
    path('pekerjaan/update/<str:pekerjaan>', views.UpdatePekerjaan, name = 'Update Pekerjaan'),
    path('pekerjaan/delete/<str:pekerjaan>', views.deletePekerjaan, name = 'Delete Pekerjaan'),
    path('bekerja/buatbekerja/', views.buatbekerja, name='Buat Bekerja'),
    path('bekerja/buatbekerja/create/<str:nama_tokoh>', views.createBekerja, name='Mulai Bekerja'),
    path('menggunakan-barang/penggunaan/', views.buatMenggunakanbarang, name='Buat Menggunakan BArang')
    ]