from django import forms
from ReadPaskalis import views
from ReadBintangNS import views as vbintang

JENIS_KELAMIN = [
    ('laki-laki', 'Laki-Laki'),
    ('perempuan', 'Perempuan'),
    ('other', 'Other'),
    ]
class FormCreateTokoh(forms.Form):
    nama_tokoh = forms.CharField(label='Nama Tokoh', max_length=20, min_length=1)
    nama_pekerjaan = forms.CharField(label='Nama Pekerjaan', max_length=10, min_length=1)
    warna_kulit = forms.CharField(label='Nama Pekerjaan', max_length=10, min_length=1)
    jenis_kelamin = forms.CharField(widget=forms.Select(choices=JENIS_KELAMIN))
