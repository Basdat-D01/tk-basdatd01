from datetime import datetime

from django.shortcuts import render
from Auth.views import *
from Auth.urls import *
from django.shortcuts import render, redirect
import django.db as db
from django.db import connection
from ReadBintangNS import forms
# Create your views here.
from ReadPaskalis import *

def GetMenggunakanBarang(request):
    userUsername = checkLoggedIn(request)


    if not userUsername:
        return redirect('mainApp:Index')
    data = request.session['username']
    role = cekAdmin(request, data)

    if not cekAdmin(request, userUsername):

        return redirect('/')

    cursor = connection.cursor()
    cursor.execute("set search_path to the_cims")
    if role == "Admin":
        cursor.execute(f"SELECT * FROM MENGGUNAKAN_BARANG AS M, KOLEKSI_JUAL_BELI AS K WHERE K.id_koleksi=M.id_barang")
        barang = cursor.fetchall()

        return render(request, 'MenggunakanBarang.html', {'barang': barang})

    elif role == "Pemain":
        cursor.execute(
            "SELECT * FROM MENGGUNAKAN_BARANG AS M, KOLEKSI_JUAL_BELI AS K WHERE K.id_koleksi=M.id_barang AND M.username_pengguna = %s", [data])
        barang = cursor.fetchall()

        print(barang)
        cursor.execute("SET SEARCH_PATH TO public")



        return render(request, 'MenggunakanBarang.html', {'barang' : barang})

def buatMenggunakanbarang(request):
    userUsername = checkLoggedIn(request)

    response = {}

    if not userUsername:
        return redirect('mainApp:Index')

    data = request.session['username']
    post = request.POST
    role = cekAdmin(request, data)

    if role == "Pemain":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")

        cursor.execute("SELECT NAMA FROM TOKOH WHERE USERNAME_PENGGUNA = %s", [data])
        listtokoh = cursor.fetchall()
        response['listtokoh'] = listtokoh

        baranglike = "BR%"

        cursor.execute("SELECT id_koleksi FROM KOLEKSI_TOKOH WHERE id_koleksi LIKE %s AND USERNAME_PENGGUNA = %s", [baranglike,data])
        list_barang = cursor.fetchall()
        response['list_barang'] = list_barang

        if request.method == 'POST':
            tokoh = post['listtokoh']
            barang = post['list_barang']

            cursor.execute("SELECT ENERGI FROM TOKOH WHERE NAMA = %s", [tokoh])
            energitokoh = cursor.fetchone()
            print(energitokoh)

            cursor.execute("SELECT TINGKAT_ENERGI FROM BARANG WHERE id_koleksi = %s", [barang])
            energibarang = cursor.fetchone()
            print(energibarang)

            timestamp = datetime.now()


            try:
                if(int(energitokoh[0]) >= int(energibarang[0])):
                    cursor.execute("INSERT INTO MENGGUNAKAN_BARANG VALUES (%s, %s, %s, %s)", [data, tokoh, timestamp, barang])
                    return redirect('/menggunakan-barang')

                else:
                    raise Exception("Error")
            except Exception:
                e = 'Energi tokoh tidak mencukupi sehingga barang tidak dapat digunakan 😭☝️☝️, ' + "Energi Tokoh : " + str(energitokoh[0]) + " , Energi yang dibutuhkan : " + str(energibarang[0])
                response['message'] = e

        return render(request, 'buatmenggunakanbarang.html', response)




def GetPekerjaan(request):
    userUsername = checkLoggedIn(request)
    response = {}

    if not userUsername:
        return redirect('mainApp:Index')

    if not cekAdmin(request, userUsername):

        return redirect('/')

    cursor = connection.cursor()
    cursor.execute("set search_path to the_cims")

    cursor.execute("SELECT * FROM PEKERJAAN")
    pekerjaan = cursor.fetchall()
    response['pekerjaan'] = pekerjaan

    return render(request, 'Pekerjaan.html', response)

def GetBekerja(request):
    userUsername = checkLoggedIn(request)


    if not userUsername:
        return redirect('mainApp:Index')

    data = request.session['username']
    role = cekAdmin(request, data)

    if role == "Pemain":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")

        cursor.execute("SELECT username_pengguna, nama_tokoh, timestamp, nama_pekerjaan, keberangkatan_ke, honor FROM bekerja where username_pengguna = %s", [data])
        bekerja = cursor.fetchall()

        return render(request, 'Bekerja.html', {'bekerja': bekerja})

    elif role == "Admin":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")

        cursor.execute("SELECT * FROM BEKERJA")
        bekerja = cursor.fetchall()

        return render(request, 'Bekerja.html', {'bekerja': bekerja})

def TambahPekerjaan(request):
    userUsername = checkLoggedIn(request)

    if not userUsername:
        return redirect('mainApp:Index')

    if not cekAdmin(request, userUsername):

        return redirect('/')

    cursor = connection.cursor()
    cursor.execute("SET search_path to the_cims")
    data = request.POST

    message = ''

    if request.method == 'POST':
        nama = data['nama']
        base_honor = data['base_honor']
        isValid = 0;

        try:
            cursor.execute("set search_path to the_cims")
            cursor.execute(
                "INSERT INTO PEKERJAAN VALUES (%s, %s)", [nama, base_honor]
            )
        except db.Error as e:
            message = e
            isValid += 1
    return render(request, 'tambahpekerjaan.html', {'message': message})

def UpdatePekerjaan(request, pekerjaan):
    userUsername = checkLoggedIn(request)


    if not userUsername:
        return redirect('mainApp:Index')

    cekrole = request.session['username']
    role = cekAdmin(request, cekrole)
    data = request.POST
    response = {}

    if role == "Admin":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")
        cursor.execute("SELECT nama FROM Pekerjaan WHERE nama=%s", [pekerjaan])
        nama_kerjaan = cursor.fetchall()
        response['nama_kerjaan'] = nama_kerjaan

        if request.method == 'POST':

            base_honor = data['base_honor']

            cursor.execute(
                "UPDATE pekerjaan set base_honor = %s WHERE nama = %s",
                [base_honor, pekerjaan]
            )
            return redirect('/pekerjaan')

    return render(request, 'UpdatePekerjaan.html', response)

def deletePekerjaan(request, pekerjaan):#Masih harus di perbaikin belom bener
    userUsername = checkLoggedIn(request)


    if not userUsername:
        return redirect('mainApp:Index')

    cekrole = request.session['username']
    role = cekAdmin(request, cekrole)
    balik = redirect('/pekerjaan')
    data = request.POST
    response = {}

    if role == "Admin":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")

        cursor.execute("SELECT NAMA FROM PEKERJAAN WHERE NAMA = %s", [pekerjaan])
        fetchpekerjaan1 = cursor.fetchall()

        cursor.execute("SELECT PEKERJAAN FROM TOKOH WHERE PEKERJAAN = %s", [pekerjaan])
        fetchpekerjaan = cursor.fetchall()

        cursor.execute("SELECT Nama_Pekerjaan FROM Bekerja WHERE nama_pekerjaan = %s", [pekerjaan])
        fetchfrombekerja = cursor.fetchall()

        cursor.execute("SELECT Nama_Pekerjaan FROM Apparel WHERE nama_pekerjaan = %s", [pekerjaan])
        fetchfromapparel = cursor.fetchall()

        if(fetchpekerjaan != []):
            if(fetchpekerjaan1[0] == fetchpekerjaan[0]):
                print("masih ada kerjaannya")
            elif(fetchpekerjaan1[0] == fetchfrombekerja[0]):
                print("masih ada kerjaannya")
            elif (fetchpekerjaan1[0] == fetchfromapparel[0]):
                print("masih ada kerjaannya")
        else:
            print("Gaada nih sung delete " + pekerjaan)
            cursor.execute("DELETE FROM PEKERJAAN WHERE nama = %s", [pekerjaan])


        return redirect('/pekerjaan')

    return render('/pekerjaan')


def GetTokohAdmin(request):
    userUsername = checkLoggedIn(request)

    if not userUsername:
        return redirect('mainApp:Index')

    if not cekAdmin(request, userUsername):

        return redirect('/')

    cursor = connection.cursor()
    cursor.execute("set search_path to the_cims")

    cursor.execute("SELECT * FROM TOKOH")
    tokohAdmin = cursor.fetchall()

    return render(request, 'ListTokohAdmin.html', {'tokohAdmin': tokohAdmin})

def GetTokohPengguna(request):
    userUsername = checkLoggedIn(request)


    if not userUsername:
        return redirect('mainApp:Index')

    data = request.session['username']
    role = cekAdmin(request, data)

    if role == "Admin":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")
        cursor.execute("SELECT * FROM TOKOH")
        tokoh = cursor.fetchall()

        return render(request, 'ListTokoh.html', {'tokoh': tokoh})

    elif role == "Pemain":

        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")

        cursor.execute("SELECT username_pengguna, nama, jenis_kelamin, status, xp, energi, kelaparan, hubungan_sosial, warna_kulit, level, sifat, pekerjaan, id_rambut, id_mata, id_rumah FROM tokoh WHERE username_pengguna = %s", [data])
        tokoh = cursor.fetchall()

        return render(request, 'ListTokoh.html', {'tokoh': tokoh})

def TambahTokoh(request): #Belom ada trigger
    userUsername = checkLoggedIn(request)


    message = ''

    if not userUsername:
        return redirect('mainApp:Index')

    cekrole = request.session['username']
    role = cekAdmin(request, cekrole)
    data = request.POST
    response = {}

    if role == "Pemain":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")

        cursor.execute("SELECT * FROM PEKERJAAN")
        pekerjaan = cursor.fetchall()
        response['pekerjaan'] = pekerjaan

        cursor.execute("SELECT * FROM WARNA_KULIT")
        warna_kulit = cursor.fetchall()
        response['warna_kulit'] = warna_kulit

    if request.method == 'POST':
        nama_tokoh = data['nama_tokoh']
        jenis_kelamin = data['jenis_kelamin']
        warna_kulit = data['warna_kulit']
        status = "Aktif"
        xp = 0
        energi = 100
        kelaparan = 0
        hubungan_sosial = 0
        pekerjaan = data['nama_pekerjaan']
        level = 1
        sifat = "Kreatif"
        id_rambut = "RB001"
        id_mata = "MT001"
        id_rumah = "RM001"



        isValid = 0;

        try:
            cursor.execute("set search_path to the_cims")
            cursor.execute(
                "INSERT INTO TOKOH VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", [cekrole, nama_tokoh, jenis_kelamin, status, xp, energi, kelaparan, hubungan_sosial, warna_kulit, level, sifat, pekerjaan, id_rambut, id_mata, id_rumah]
            )
            cursor.close()
        except db.Error as e:
            message = e
            isValid += 1
            
        return render(request, 'CreateTokoh.html', {'message': message})

    return render(request, 'CreateTokoh.html', response)

def UpdateTokoh(request, nama_tokoh):
    userUsername = checkLoggedIn(request)


    message = ''

    if not userUsername:
        return redirect('mainApp:Index')

    cekrole = request.session['username']
    role = cekAdmin(request, cekrole)
    data = request.POST
    response = {}

    if role == "Pemain":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")

        cursor.execute("SELECT NAMA FROM TOKOH WHERE NAMA=%s", [nama_tokoh])
        namatokoh = cursor.fetchall()
        response['namatokoh'] = namatokoh
        print(namatokoh)

        rmbt = "RB%"
        mt = "MT%"
        rm = "RM%"

        cursor.execute("SELECT id_koleksi FROM KOLEKSI_TOKOH WHERE id_koleksi LIKE %s AND NAMA_TOKOH = %s", [rmbt,nama_tokoh])
        idRambut = cursor.fetchall()
        response['idRambut'] = idRambut

        cursor.execute("SELECT id_koleksi FROM KOLEKSI_TOKOH WHERE id_koleksi LIKE %s AND NAMA_TOKOH = %s", [mt,nama_tokoh])
        idMata = cursor.fetchall()
        response['idMata'] = idMata

        cursor.execute("SELECT id_koleksi FROM KOLEKSI_TOKOH WHERE id_koleksi LIKE %s AND NAMA_TOKOH = %s", [rm,nama_tokoh])
        idRumah = cursor.fetchall()
        response['idRumah'] = idRumah

        if request.method == 'POST':
            data = request.POST
            id_Rambut = data['idRambut']

            id_Mata = data['idMata']


            id_Rumah = data['idRumah']
            cursor.execute(
                "UPDATE tokoh set id_rambut = %s, id_mata = %s, id_rumah= %s WHERE NAMA = %s", [id_Rambut, id_Mata, id_Rumah, namatokoh]
            )
            return redirect('/list-tokoh')
    return render(request, 'updateTokoh.html', response)

def DetailTokohPengguna(request, nama_tokoh):
    userUsername = checkLoggedIn(request)

    if not userUsername:
        return redirect('mainApp:Index')

    cekrole = request.session['username']
    role = cekAdmin(request, cekrole)
    response = {}

    if role == "Pemain":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")

        cursor.execute("SELECT NAMA, ID_RAMBUT, ID_MATA, ID_RUMAH, WARNA_KULIT, PEKERJAAN FROM TOKOH WHERE USERNAME_PENGGUNA=%s", [nama_tokoh])
        detailtokoh = cursor.fetchall()


        return render(request, 'detail_tokoh.html', {'detailtokoh': detailtokoh})

    return redirect('/list-tokoh')
def buatbekerja(request):
    userUsername = checkLoggedIn(request)
    if not userUsername:
        return redirect('mainApp:Index')

    cekrole = request.session['username']
    role = cekAdmin(request, cekrole)
    response = {}

    if role == "Pemain":

        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")

        cursor.execute("SELECT TOKOH.nama, TOKOH.pekerjaan, PEKERJAAN.base_honor FROM TOKOH, PEKERJAAN WHERE TOKOH.username_pengguna = %s AND PEKERJAAN.nama = TOKOH.pekerjaan GROUP BY(TOKOH.nama, TOKOH.pekerjaan, PEKERJAAN.nama)", [cekrole])

        result = cursor.fetchall()
        response['result'] = result
        return render(request, 'buatbekerja.html', {'result': result})
    return redirect('/')

def createBekerja(request, nama_tokoh):
    userUsername = checkLoggedIn(request)
    if not userUsername:
        return redirect('mainApp:Index')

    cekrole = request.session['username']
    role = cekAdmin(request, cekrole)
    response = {}



    if role == "Pemain":


        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")


        cursor.execute(f"""SELECT MAX(keberangkatan_ke) FROM BEKERJA WHERE nama_tokoh = '{nama_tokoh}'
                        """)
        jumlah_keberangkatan = cursor.fetchone()

        timestamp = datetime.now()
        x = timestamp.strftime("%Y-%m-%d %H:%M:%S")

        if(jumlah_keberangkatan[0] == None):
            jumlah_keberangkatan = 1
        else:
            jumlah_keberangkatan = int(jumlah_keberangkatan[0])+1

        cursor.execute("SELECT NAMA FROM TOKOH WHERE USERNAME_PENGGUNA=%s", [cekrole])
        nama_tokoh = cursor.fetchone()
        response['nama_tokoh'] = nama_tokoh

        cursor.execute("SELECT NAMA_TOKOH FROM BEKERJA WHERE NAMA_TOKOH = %s", [nama_tokoh[0]])
        ceknamatokoh = cursor.fetchone()


        cursor.execute("SELECT LEVEL FROM TOKOH WHERE NAMA = %s", [nama_tokoh[0]])
        level = cursor.fetchall()
        response['level'] = level

        cursor.execute("SELECT PEKERJAAN FROM TOKOH WHERE NAMA =%s", [nama_tokoh[0]])
        pekerjaan_tokoh = cursor.fetchall()
        response['pekerjaan_tokoh']= pekerjaan_tokoh

        cursor.execute("SELECT base_honor FROM PEKERJAAN WHERE nama = %s", [pekerjaan_tokoh[0]])
        base_honor = cursor.fetchall()
        response['base_honor'] = base_honor

        base = str(base_honor[0][0])
        lev = str(level[0][0])

        base_honortambah = int(base) * int(lev)
        print(ceknamatokoh)
        if(ceknamatokoh == None):
            cursor.execute("INSERT INTO BEKERJA VALUES (%s, %s, %s, %s, %s, %s)", [cekrole, nama_tokoh[0], timestamp, pekerjaan_tokoh[0], jumlah_keberangkatan, base_honortambah])
            print("Masuk If")
        else:
            cursor.execute("UPDATE BEKERJA SET TIMESTAMP = %s, KEBERANGKATAN_KE = %s, HONOR = %s WHERE NAMA_TOKOH = %s", [x, jumlah_keberangkatan, base_honortambah, nama_tokoh[0]])
            print("Masuk else")
        return redirect("/bekerja")
    return redirect("/bekerja")

















