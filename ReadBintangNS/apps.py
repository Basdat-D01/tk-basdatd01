from django.apps import AppConfig


class ReadbintangnsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ReadBintangNS'
