from datetime import datetime
from urllib import response
from django.shortcuts import render
from Auth.views import *
from Auth.urls import *
from django.shortcuts import render, redirect
import django.db as db
from django.db import connection
# Create your views here.

def readMisiUtama(request):
    userUsername = checkLoggedIn(request)

    if not userUsername:
        return redirect('mainApp:Index')

    cursor = connection.cursor()
    cursor.execute("set search_path to the_cims")

    cursor.execute("SELECT * FROM MISI_UTAMA")
    MisiUtama = cursor.fetchall()

    cursor.execute("SELECT * FROM MISI_UTAMA WHERE MISI_UTAMA.Nama_Misi NOT IN (SELECT Nama_misi FROM MENJALANKAN_MISI_UTAMA)")
    MisiUtamaNonDeletable = cursor.fetchall()
    print(MisiUtamaNonDeletable)
    return render(request, 'MisiUtama.html', {'MisiUtama': MisiUtama, 'MisiUtamaNonDeletable': MisiUtamaNonDeletable})

def readMenjalankanMisiUtama(request):
    userUsername = checkLoggedIn(request)
    data = request.session['username']
    role = cekAdmin(request, data)

    if not userUsername:
        return redirect('mainApp:Index')

    if role == "Pemain":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")

        cursor.execute("SELECT Nama_tokoh, Nama_misi, Status FROM MENJALANKAN_MISI_UTAMA WHERE username_pengguna = %s", [data])
        MenjalankanMisiUtama = cursor.fetchall()

    elif role == "Admin":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")

        cursor.execute("SELECT * FROM MENJALANKAN_MISI_UTAMA")
        MenjalankanMisiUtama = cursor.fetchall()

    return render(request, 'MenjalankanMisiUtama.html', {'MenjalankanMisiUtama': MenjalankanMisiUtama})

def readMakanan(request):
    userUsername = checkLoggedIn(request)
    data = request.session['username']
    role = cekAdmin(request, data)

    if not userUsername:
        return redirect('mainApp:Index')

    cursor = connection.cursor()
    cursor.execute("set search_path to the_cims")

    cursor.execute("SELECT * FROM MAKANAN")
    Makanan = cursor.fetchall()

    return render(request, 'Makanan.html', {'Makanan': Makanan})

def readMakan(request):
    userUsername = checkLoggedIn(request)
    data = request.session['username']
    role = cekAdmin(request, data)

    if not userUsername:
        return redirect('mainApp:Index')

    if role == "Pemain":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")

        cursor.execute("SELECT Nama_tokoh, Waktu, Nama_makanan FROM MAKAN where username_pengguna = %s", [data])
        Makan = cursor.fetchall()

    elif role == "Admin":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")

        cursor.execute("SELECT * FROM MAKAN")
        Makan = cursor.fetchall()

    return render(request, 'Makan.html', {'Makan': Makan})

def createMisiUtama(request):
    userUsername = checkLoggedIn(request)
    data = request.session['username']
    post = request.POST
    role = cekAdmin(request, data)

    message = ''

    if not userUsername:
        return redirect('mainApp:Index')

    if role == "Admin":
        cursor = connection.cursor()
        if request.method == 'POST':
            Nama = post['Nama']
            Efek_Energi = post['Efek_Energi']
            Efek_Hubungan_sosial = post['Efek_Hubungan_sosial']
            Efek_Kelaparan = post['Efek_Kelaparan']
            Syarat_Energi = post['Syarat_Energi']
            Syarat_Hubungan_sosial = post['Syarat_Hubungan_sosial']
            Syarat_Kelaparan = post['Syarat_Kelaparan']
            Completion_time = post['Completion_time']
            Reward_koin = post['Reward_koin']
            Reward_xp = post['Reward_xp']
            Deskripsi = post['Deskripsi']
            isValid = 0
            
            try:
                cursor.execute("set search_path to the_cims")
                cursor.execute(
                    "INSERT INTO MISI VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", 
                [Nama, Efek_Energi, Efek_Hubungan_sosial, Efek_Kelaparan, 
                Syarat_Energi, Syarat_Hubungan_sosial, Syarat_Kelaparan,
                Completion_time, Reward_koin, Reward_xp, Deskripsi]
                )
                cursor.execute(
                    "INSERT INTO MISI_UTAMA VALUES (%s)", [Nama]
                )
            except db.Error as e:
                message = e
                isValid =+ 1

            if isValid == 0:
                return redirect('/misi-utama/')

    return render(request, 'CreateMisiUtama.html', {'message' : message})

def createMenjalankanMisiUtama(request):
    userUsername = checkLoggedIn(request)
    data = request.session['username']
    post = request.POST
    role = cekAdmin(request, data)
    response = {}

    if not userUsername:
        return redirect('mainApp:Index')

    if role == "Pemain":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")
        
        cursor.execute("SELECT Nama FROM TOKOH WHERE Username_pengguna = %s", [data])
        Nama = cursor.fetchall()
        response['Nama'] = Nama

        cursor.execute("SELECT * FROM MISI_UTAMA")
        Nama_Misi = cursor.fetchall()
        response['Nama_Misi'] = Nama_Misi

        if request.method == 'POST':
            Nama_tokoh = post['Nama']
            Nama_misi = post['Nama_Misi']

            cursor.execute(
                    "INSERT INTO MENJALANKAN_MISI_UTAMA VALUES (%s, %s, %s, %s)", [data, Nama_tokoh, Nama_misi, "Aktif"]
                )
           
            return redirect('/menjalankan-misi-utama/')

    return render(request, 'CreateMenjalankanMisiUtama.html', response)

def createMakanan(request):
    userUsername = checkLoggedIn(request)
    data = request.session['username']
    post = request.POST
    role = cekAdmin(request, data)

    message = ''

    if not userUsername:
        return redirect('mainApp:Index')

    if role == "Admin":
        cursor = connection.cursor()
        if request.method == 'POST':
            Nama = post['Nama']
            Harga = post['Harga']
            Tingkat_energi = post['Tingkat_energi']
            Tingkat_kelaparan = post['Tingkat_kelaparan']
            isValid = 0
            
            try:
                cursor.execute("set search_path to the_cims")
                cursor.execute(
                    "INSERT INTO MAKANAN VALUES (%s, %s, %s, %s)", 
                [Nama, Harga, Tingkat_energi, Tingkat_kelaparan]
                )
            except db.Error as e:
                message = e
                isValid =+ 1

            if isValid == 0:
                return redirect('/makanan/')

    return render(request, 'CreateMakanan.html', {'message' : message})

def createMakan(request):
    userUsername = checkLoggedIn(request)
    data = request.session['username']
    post = request.POST
    role = cekAdmin(request, data)
    response = {}

    if not userUsername:
        return redirect('mainApp:Index')

    if role == "Pemain":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")
        
        cursor.execute("SELECT Nama FROM TOKOH WHERE Username_pengguna = %s", [data])
        Nama = cursor.fetchall()
        response['Nama'] = Nama

        cursor.execute("SELECT * FROM MAKANAN")
        Nama_Makanan = cursor.fetchall()
        response['Nama_Makanan'] = Nama_Makanan

        if request.method == 'POST':
            Nama_tokoh = post['Nama']
            Nama_makanan = post['Nama_Makanan']

            cursor.execute(
                    "INSERT INTO MAKAN VALUES (%s, %s, %s, %s)", [data, Nama_tokoh, datetime.now(), Nama_makanan]
                )
           
            return redirect('/makan/')

    return render(request, 'CreateMakan.html', response)

def detailMisiUtama(request, Nama_Misi):
    userUsername = checkLoggedIn(request)

    if not userUsername:
        return redirect('mainApp:Index')

    cursor = connection.cursor()
    cursor.execute("set search_path to the_cims")

    cursor.execute("SELECT * FROM MISI, MISI_UTAMA WHERE MISI.Nama = MISI_UTAMA.Nama_Misi AND MISI.Nama = %s", [Nama_Misi])
    DetailMisiUtama = cursor.fetchall()

    return render(request, 'DetailMisiUtama.html', {'DetailMisiUtama': DetailMisiUtama})

def updateMenjalankanMisiUtama(request, Nama_Tokoh, Nama_Misi):
    userUsername = checkLoggedIn(request)
    data = request.session['username']
    post = request.POST
    role = cekAdmin(request, data)
    response = {}

    if not userUsername:
        return redirect('mainApp:Index')

    if role == "Pemain":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")
        
        cursor.execute("SELECT DISTINCT(Nama_tokoh) FROM MENJALANKAN_MISI_UTAMA WHERE Nama_tokoh = %s", [Nama_Tokoh])
        namaTokoh = cursor.fetchall()
        response['namaTokoh'] = namaTokoh

        cursor.execute("SELECT DISTINCT(Nama_misi) FROM MENJALANKAN_MISI_UTAMA WHERE Nama_misi = %s", [Nama_Misi])
        namaMisi = cursor.fetchall()
        response['namaMisi'] = namaMisi

        cursor.execute("SELECT Status FROM MENJALANKAN_MISI_UTAMA WHERE Nama_tokoh = %s and Nama_misi = %s", [Nama_Tokoh[0], Nama_Misi[0]])
        Status = cursor.fetchall()
        response['Status'] = Status

        if request.method == 'POST':
            status = post['Status']

            cursor.execute(
                    "UPDATE MENJALANKAN_MISI_UTAMA SET Status = %s WHERE Nama_tokoh = %s and Nama_misi = %s", [status, namaTokoh[0], namaMisi[0]]
                )
           
            return redirect('/menjalankan-misi-utama/')

    return render(request, 'UpdateMenjalankanMisiUtama.html', response)

def updateMakanan(request, Nama_Makanan):
    userUsername = checkLoggedIn(request)
    data = request.session['username']
    post = request.POST
    role = cekAdmin(request, data)
    response = {}

    if not userUsername:
        return redirect('mainApp:Index')

    if role == "Admin":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")
        
        cursor.execute("SELECT DISTINCT(Nama) FROM MAKANAN WHERE Nama = %s", [Nama_Makanan])
        namaMakanan = cursor.fetchall()
        response['namaMakanan'] = namaMakanan

        if request.method == 'POST':
            Harga = post['Harga']
            Tingkat_energi = post['Tingkat_energi']
            Tingkat_kelaparan = post['Tingkat_kelaparan']

            cursor.execute(
                    "UPDATE MAKANAN SET Harga = %s, Tingkat_energi = %s, Tingkat_kelaparan = %s WHERE Nama = %s", [Harga, Tingkat_energi, Tingkat_kelaparan, namaMakanan[0]]
                )
           
            return redirect('/makanan/')

    return render(request, 'UpdateMakanan.html', response)
    
def deleteMisiUtama(request, Nama_Misi):
    userUsername = checkLoggedIn(request)

    if not userUsername:
        return redirect('mainApp:Index')

    data = request.session['username']
    role = cekAdmin(request, data)

    if role == "Admin":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")

        cursor.execute("SELECT Nama_Misi FROM MISI_UTAMA WHERE Nama_Misi = %s", [Nama_Misi])
        fetchMisiUtama = cursor.fetchall()

        cursor.execute("SELECT Nama_misi FROM MENJALANKAN_MISI_UTAMA WHERE Nama_misi = %s", [Nama_Misi])
        fetchMenjalankanMisiUtama = cursor.fetchall()

        if (fetchMenjalankanMisiUtama == []):
            cursor.execute("DELETE FROM MISI WHERE Nama = %s", [Nama_Misi])
        elif (fetchMenjalankanMisiUtama[0] == fetchMisiUtama[0]):
            None

        return redirect('/misi-utama/')

    return render('/misi-utama/')

def deleteMakanan(request, Nama_Makanan):
    userUsername = checkLoggedIn(request)

    if not userUsername:
        return redirect('mainApp:Index')

    data = request.session['username']
    role = cekAdmin(request, data)

    if role == "Admin":
        cursor = connection.cursor()
        cursor.execute("set search_path to the_cims")

        cursor.execute("SELECT Nama FROM MAKANAN WHERE Nama = %s", [Nama_Makanan])
        fetchMakanan = cursor.fetchall()

        cursor.execute("SELECT Nama_makanan FROM MAKAN WHERE Nama_makanan = %s", [Nama_Makanan])
        fetchMakan = cursor.fetchall()

        if (fetchMakan == []):
            cursor.execute("DELETE FROM MAKANAN WHERE Nama = %s", [Nama_Makanan])
        elif (fetchMakan[0] == fetchMakanan[0]):
            None

        return redirect('/makanan/')

    return render('/makanan/')
