from django.apps import AppConfig


class ReadbismaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'ReadBisma'
