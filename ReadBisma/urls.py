from ReadBisma import views
from django.urls import path

app_name = 'ReadBisma'
urlpatterns = [
    path('misi-utama/', views.readMisiUtama, name = 'Misi Utama'),
    path('misi-utama/detail/<str:Nama_Misi>', views.detailMisiUtama, name = 'Detail Misi Utama'),
    path('misi-utama/delete/<str:Nama_Misi>', views.deleteMisiUtama, name = 'Delete Misi Utama'),
    path('menjalankan-misi-utama/', views.readMenjalankanMisiUtama, name = 'Menjalankan Misi Utama'),
    path('menjalankan-misi-utama/update/<str:Nama_Tokoh>/<str:Nama_Misi>', views.updateMenjalankanMisiUtama, name = 'Update Menjalankan Misi Utama'),
    path('makanan/', views.readMakanan, name = 'Makanan'),
    path('makanan/update/<str:Nama_Makanan>', views.updateMakanan, name = 'Update Makanan'),
    path('makanan/delete/<str:Nama_Makanan>', views.deleteMakanan, name = 'Delete Makanan'),
    path('makan/', views.readMakan, name = 'Makan'),
    path('create-misi-utama/', views.createMisiUtama, name = 'Create Misi Utama'),
    path('create-menjalankan-misi-utama/', views.createMenjalankanMisiUtama, name = 'Create Menjalankan Misi Utama'),
    path('create-makan/', views.createMakan, name = 'Create Makan'),
    path('create-makanan/', views.createMakanan, name = 'Create Makan'),
    ]