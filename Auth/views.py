import os
import psycopg2
from django.shortcuts import render, redirect
import django.db as db
from django.db import connection
from django.contrib.auth import logout


# Create your views here.

def index(request):
    cursor = connection.cursor()
    cursor.execute("set search_path to public")

    username = ''
    try:
        username = request.session['username']
    except:
        username = ''

    return render(request, 'Index.html', {'username': username})


def login(request):
    data = request.POST
    cursor = connection.cursor()
    cursor.execute("set search_path to the_cims")

    message = ''

    if request.method == 'POST':
        username = data['username']
        password = data['password']
        cursor.execute(
            "select success.username, success.password from akun,(select username, password from pemain union select username, password from admin) as success where akun.username = success.username and success.username = %s and success.password = %s",
            [username, password])
        role = cekAdmin(request, username)
        Admin = False
        Pemain = False

        if(role == 'Admin'):
            Admin = True
        if(role == 'Pemain'):
            Pemain = True

        if cursor.rowcount == 0:
            message = 'Username tidak terdaftar pada sistem atau password salah'
        else:
            cursor.execute("set search_path to public")
            request.session['username'] = username
            request.session['role'] = role

            return redirect('/')

    return render(request, 'login.html', {'message': message})



def logout_user(request):
    logout(request)

    return redirect('mainApp:Index')

def register(request):
    return render(request, 'register.html')

def register_user(request):
    data = request.POST
    cursor = connection.cursor()
    cursor.execute("set search_path to the_cims")



    message = ''

    if request.method == 'POST':
        username = data['username']
        email = data['email']
        password = data['password']
        nohp = data['noHP']
        # koin = data['koin']
        isValid = 0;

        cursor.execute("SELECT EMAIL FROM PEMAIN WHERE EMAIL = %s", [email])
        getemail = cursor.fetchone()

        cursor.execute("SELECT USERNAME FROM AKUN WHERE USERNAME = %s", [username])
        getusername = cursor.fetchone()
        print(getemail)
        print(getusername)

        try:
            if getemail is None and getusername is None:
                cursor.execute("set search_path to the_cims")
                cursor.execute(
                    "INSERT INTO AKUN VALUES (%s)", [username]
                )
            else:
                raise Exception
        except Exception as e:
            message = "Email atau Username sudah terdaftar dalam sistem"
            isValid += 1

        insertInvalid = 0

        if isValid == 0:
            try:
                cursor.execute("INSERT INTO PEMAIN VALUES (%s, %s, %s, %s, %s)", [username, email, password, nohp, 0])


            except db.Error as e:
                message = e
                insertInvalid += 1

            if insertInvalid == 0:
                cursor.execute("select success.username, success.password from akun,(select username, password from pemain union select username, password from admin) as success where akun.username = success.username and success.username = %s and success.password = %s",
                [username, password])
                role = cekAdmin(request, username)
                cursor.execute("set search_path to public")
                request.session['username'] = username
                request.session['role'] = role

                return redirect('/')

    return render(request, 'register_user.html', {'message': message})

def register_admin(request):
    data = request.POST
    cursor = connection.cursor()
    cursor.execute("SET search_path to the_cims")


    message = ''
    if request.method == 'POST':
        username = data['username']
        password = data['password']
        isValid = 0


        cursor.execute("SELECT USERNAME FROM AKUN WHERE USERNAME = %s", [username])
        getusername = cursor.fetchone()


        try:
            if getusername is None:
                cursor.execute(
                    "INSERT INTO AKUN VALUES (%s)", [username]
                )
            else:
                raise Exception
        except Exception as e:
            message = "Admin dengan username ini sudah terdaftar dalam sistem"
            isValid += 1

        insertInvalid = 0

        # if isValid == 0:
        if isValid == 0:
            try:
                cursor.execute("INSERT INTO ADMIN VALUES (%s, %s)", [username, password])

            except db.Error as e:
                message = e
                insertInvalid += 1

            if insertInvalid == 0:
                cursor.execute(
                    "select success.username, success.password from akun,(select username, password from pemain union select username, password from admin) as success where akun.username = success.username and success.username = %s and success.password = %s",
                    [username, password])
                role = cekAdmin(request, username)
                cursor.execute("set search_path to public")
                request.session['username'] = username
                request.session['role'] = role
                return redirect('/')

    return render(request, 'register_admin.html', {'message':message})

def cekAdmin(request, username):
    Admin = False
    cursor = connection.cursor()
    cursor.execute("SET search_path to the_cims")

    cursor.execute("SELECT username from admin where username = %s", [username])
    role = cursor.fetchall()
    if(role != []):
        cursor.close()
        return "Admin"
    else:
        cursor.execute("SELECT username from pemain where username = %s", [username])
        pemain = cursor.fetchall()
        if(pemain != []):
            cursor.close()
            return "Pemain"


def checkLoggedIn(request):
    cursor = connection.cursor()

    # check if not logged in
    userEmail = ''
    try:
        cursor.execute("set search_path to public")
        userEmail = request.session['username']
    except:
        return False
    return userEmail

def ProfilePemain(request):
    userUsername = checkLoggedIn(request)

    if not userUsername:
        return redirect('mainApp:Index')


    data = request.session['username']

    cursor = connection.cursor()
    cursor.execute("set search_path to the_cims")
    cursor.execute( "SELECT username, email, no_hp, koin from pemain where username = %s ", [data])
    profile = cursor.fetchall()

    return render(request, 'profile.html', {'profile': profile})

def ProfileAdmin(request):
    userUsername = checkLoggedIn(request)

    if not userUsername:
        return redirect('mainApp:Index')

    data = request.session['username']

    cursor = connection.cursor()
    cursor.execute("set search_path to the_cims")
    cursor.execute("SELECT username from Admin where username = %s ", [data])
    profileAdmin = cursor.fetchall()

    return render(request, 'profile_admin.html', {'profileAdmin': profileAdmin})






