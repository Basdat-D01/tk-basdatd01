from Auth import views
from django.urls import path

app_name = 'Auth'
urlpatterns = [
    path('', views.index, name = 'Index'),
    path('login/', views.login, name = 'Login'),
    path('register/', views.register, name = 'Register'),
    path('register/user/', views.register_user, name='Register User'),
    path('register/admin/', views.register_admin, name='Register Admin'),
    path('logout/', views.logout_user, name = 'Logout'),
    path('profile/', views.ProfilePemain, name = 'Profile'),
    path('profile-admin/', views.ProfileAdmin, name = 'ProfileAdmin')
    ]