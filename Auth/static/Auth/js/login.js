//$(document).ready(() => {
//  let valid_username = false;
//  let valid_password = false;
//
//  $("#username").on("input", () =>
//    validateInput("username", $("#username").val())
//  );
//
//  $("#password").on("input", () =>
//    validateInput("password", $("#password").val())
//  );
//
//  $("#signin").on("click", () => {
//    $(".error-message").remove();
//    if (valid_username && valid_password) {
//      const username = $("#username").val();
//      const password = $("#password").val();
//      login(username, password);
//    }
//  });
//
//  function validateInput(mode, value) {
//    if (value.length == 0) {
//      changeValidityState(mode, false);
//    } else {
//      changeValidityState(mode, true);
//    }
//    setSubmitButton();
//  }
//
//  function setSubmitButton() {
//    if (valid_username && valid_password) {
//      $("#signin").css({
//        "pointer-events": "visible",
//        cursor: "pointer",
//        "background-color": "#FF0000",
//      });
//    } else {
//      $("#signin").css({
//        "pointer-events": "none",
//        cursor: "default",
//        "background-color": "#acacac",
//      });
//    }
//  }
//
//  function changeValidityState(mode, state) {
//    if (mode == "username") valid_username = state;
//    if (mode == "password") valid_password = state;
//  }
//
//  // https://stackoverflow.com/questions/23349883/how-to-pass-csrf-token-to-javascript-file-in-django
//  function getCookie(name) {
//    var cookieValue = null;
//    if (document.cookie && document.cookie != "") {
//      var cookies = document.cookie.split(";");
//      for (var i = 0; i < cookies.length; i++) {
//        var cookie = cookies[i].trim();
//        // Does this cookie string begin with the name we want?
//        if (cookie.substring(0, name.length + 1) == name + "=") {
//          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
//          break;
//        }
//      }
//    }
//    return cookieValue;
//  }
//
//  function login(username, password) {
//    const csrf = getCookie("csrftoken");
//    $.ajax({
//      url: "/login/",
//      type: "POST",
//      dataType: "json",
//      data: {
//        username: username,
//        password: password,
//        csrfmiddlewaretoken: csrf,
//      },
//      success: (data, status) => {
//        console.log(data);
//        if (data.error) {
//          const error = $("<p></p>")
//            .addClass("error-message fade mb-3")
//            .text(data.error);
//          $("#login-form").prepend(error);
//        } else {
//          window.location.replace("/home/");
//        }
//      },
//      error: (error) => {
//        console.log(error);
//      },
//    });
//  }
//});
