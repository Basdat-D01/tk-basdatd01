$(document).ready(() => {
  let valid_no_hp = false;
  let valid_username = false;
  let valid_email = false;
  let valid_password1 = false;

  $("#valid_no_hp").on("input", () =>
    validateInput(
      "valid_no_hp",
      $("#valid_no_hp"),
      isName,
      $("#valid_no_hp").val()
    )
  );


  $("#username").on("input", () =>
    validateInput("username", $("#username"), isUsername, $("#username").val())
  );

  $("#email").on("input", () =>
    validateInput("email", $("#email"), isEmail, $("#email").val())
  );

  $("#password1").on("input", () =>
    validateInput(
      "password1",
      $("#password1"),
      isPassword,
      $("#password1").val()
    )
  );


  function validateInput(mode, selector, validator, value) {
    if (value.length == 0) {
      selector.css("border", "1px solid black");
      changeValidityState(mode, false);
    } else if (validator(value)) {
      selector.css("border", "1px solid black");
      changeValidityState(mode, true);
    } else {
      selector.css("border", "1px solid red");
      changeValidityState(mode, false);
    }
    setSubmitButton();
  }

  function setSubmitButton() {
    if (
      valid_no_hp &&
      valid_username &&
      valid_email &&
      valid_password1 &&
    ) {
      $("#signup").css({
        "pointer-events": "visible",
        cursor: "pointer",
        "background-color": "#FF0000",
      });
    } else {
      $("#signup").css({
        "pointer-events": "none",
        cursor: "default",
        "background-color": "#acacac",
      });
    }
  }

  // https://stackoverflow.com/questions/23349883/how-to-pass-csrf-token-to-javascript-file-in-django
  function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != "") {
      var cookies = document.cookie.split(";");
      for (var i = 0; i < cookies.length; i++) {
        var cookie = cookies[i].trim();
        // Does this cookie string begin with the name we want?
        if (cookie.substring(0, name.length + 1) == name + "=") {
          cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
          break;
        }
      }
    }
    return cookieValue;
  }

  $("#signup").on("click", () => {
    $(".error-messages").remove();
    if (
      valid_no_hp &&
      valid_username &&
      valid_email &&
      valid_password1 &&
    ) {
      const csrf = getCookie("csrftoken");
      const data = {
        first_name: $("#no_hp").val(),
        username: $("#username").val(),
        email: $("#email").val(),
        password1: $("#password1").val(),
        csrfmiddlewaretoken: csrf,
      };
      register(data);
    }
  });

  function register(data) {
    $.ajax({
      url: "/register/",
      type: "POST",
      dataType: "json",
      data: data,
      success: (data, status) => {
        console.log(data);
        if (data.error) {
          // console.log(data.error);
          const errors = $("<div></div>").addClass("error-messages");
          data.error.errors.forEach((error_message) => {
            const error = $("<p></p>")
              .addClass("error-message fade mb-3")
              .text(error_message);
            errors.append(error);
          });
          $("#register-form").prepend(errors);
        } else {
          window.location.replace("/login/");
        }
      },
      error: (error) => {
        console.log(error);
      },
    });
  }

  function isName(name) {
    var regex = /^[a-zA-Z]+$/;
    return regex.test(name);
  }

  // https://stackoverflow.com/questions/9628879/javascript-regex-username-validation
  function isUsername(username) {
    var regex = /^[a-zA-Z0-9_]+$/;
    return regex.test(username);
  }

  // https://stackoverflow.com/questions/2507030/email-validation-using-jquery
  function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }

  // https://stackoverflow.com/questions/19605150/regex-for-password-must-contain-at-least-eight-characters-at-least-one-number-a
  function isPassword(password) {
    var regex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
    return regex.test(password);
  }

  function samePassword(password1, password2) {
    return password1 == password2;
  }

  function changeValidityState(mode, state) {
    if (mode == "no_hp") valid_no_hp = state;
    if (mode == "username") valid_username = state;
    if (mode == "email") valid_email = state;
    if (mode == "password1") valid_password1 = state;
  }
});
